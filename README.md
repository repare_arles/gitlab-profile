# Repare Arles

Groupe Gitlab du repare café Arlésien. Soutenu par Kikassi-Kikassa, Zéro déchet pays d'Arles et la CIA. 👥

## C'est quoi un répare café ? 🛠️

C'est un atelier participatif qui à pour but de redonner vie à vos objets avec l'aide de nos bénévoles réparateur.ice.s. 🧰

**Chaque objet réparé, c'est un déchet évité ! ♻️**

Ainsi si vous avez un objet cassé apportez le à nos bénévoles, nous serons heureux de vous faire participer à sa remise en état et de vous donner des explications, des conseils etc. Vous n'avez rien à réparer ? Vous êtes les bienvenu.e.s aussi. Vous pourrez également profiter de ce moment convivial pour observer les réparations en cours, discuter avec l'équipe, boire un verre, manger une gourmandise ou faire un peu de lecture sur le bricolage. 📚

**Nous organisons un Répare-Café tout les mois environ. 🗓️**

## Qui sommes nous ? ❓

Le projet d'organiser des Répare-Cafés à Arles est né en 2018. Ces évènements étaient alors organisés par différentes associations dont **Zéro Déchets Pays d'Arles.** 🌍

En 2022/2023, ZDPA s'est rapprochée de Kikassi Kikassa, la bibliothèque d'objet de la ville. Ensemble, nos deux associations ont relancé une dynamique autour de cette idée simple : Mieux vaut réparer que gaspiller ! À présent, nous comptons une dizaine de bénévoles. 🤝

**En 2023, nous avons organisé 5 répares-cafés. 🎉**

## Mais pourquoi ? ❓

La réparation participe à la **réduction de l'extraction de ressources naturelles** et à la **réduction de la quantité de déchets** que nous produisons. 🌱

Le développement de la réparation figure comme **une priorité** dans la loi de 2015 relative à la transition (LTECV) ainsi que pour l'Agence de l'environnement et de la maîtrise de l'énergie (Ademe). ♻️

A ce titre les répare cafés :

- Permettent de promouvoir un "réflexe réparation" en offrant une proposition locale et accessible financièrement. 💡
- Créent du lien social et de la solidarité ❤️
- Font connaître et mettent en lien les acteurs du réemploi et de la réparation à Arles et dans les environs 🔗
- Promeuvent une économie vertueuse, en lien avec le défi de transition écologique et de la réduction de nos déchets (Refuser les déchets évitables, Réduire nos besoins, Réutiliser, Recycler, Rendre à la terre) 🌍

## Notre site web 🌐

Vous pouvez retrouver toutes les informations sur nos évènements, nos actualités et nos partenaires sur notre site web : [reparecafe-arles.fr](https://reparecafe-arles.fr) 🖥️